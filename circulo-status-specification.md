
# Circulo Status Specification 1.0

## Room (Circles)
- Each Circle is a Matrix room
- Creator of the room is the Admin
- Other users can be made moderators

## Room Info
- Each user in the room can have two personal threads: Alert and Location (for continued location tracking)
- Otherwise, any events posted in the room are displayed as just part of the discussion

## Thread Types

### Alert Status THREAD
- key: first issue `m.text` event type - "#urgent" as text: https://spec.matrix.org/latest/client-server-api/#mtext
- seen by: quick response emoji (event reaction/annotation) on the root thread event https://spec.matrix.org/latest/client-server-api/#mreaction 
- next event: `m.audio`, if user recorded any audio: https://spec.matrix.org/latest/client-server-api/#maudio
- A location sharing thread is created, if not already existing and location sharing is started, if not running, yet.
- An already existing location sharing thread is continued and not stopped, before the alert is marked resolved again.
- If an existing location sharing thread should continue after an alert was stopped, the location sharing will continue until the published end timestamp.
- When the user closes an alert, 
  - the alert thread will be marked resolved using a green checkmark quick reaction emoji (✅) and
  - the location tracking will be stopped, *if* there's not location sharing going on from before.

#### Sample Thread

Root Event: `m.text`: "#urgent"
- Reply: `m.audio` (5 second audio recording m4a)

### Location Status THREAD
- key: first issue Location event type
- First reply: `m.text` containing an ISO 8601 timestamp when the location sharing is supposed to end. (optional)
- seen by: quick response emoji (event reaction/annotation) on the thread root event https://spec.matrix.org/latest/client-server-api/#mreaction 
- Location event type: https://spec.matrix.org/latest/client-server-api/#mlocation
- Should only update with a change > 10M
- Receiving client should leniently search for the first `m.text` reply event for a timestamp, it might not be the first reply.
- When the defined end of location tracking arrived, location threads should be left untouched, until the user closes them explicitly in the UI.
- When that happens, however, *all* old location threads of that user should be redacted.

#### Sample Thread

Root Event: `m.location` "10 main street"
- Reply: `m.text`: "2024-12-31T23:59:59+00:00"
- Reply: `m.location`
- Reply: `m.location`

### Chat Status
- Display the entire room as a typical matrix discussion, include any Alert or Location thread root items.


## Active has-seen Indication
- Clients should respond with a `m.reaction` to a thread root event to indicate, that a user has seen it, when the user actively interacts with it.
- Reading client should use eyeballs 👀 emoji as quick reaction by default.
- Sending client should accept any quick reaction as a "has-seen" indicator. (Emojis might be used later to enhance the UI.)


## Redaction
- Server will be set up to drop all room history which is older than 1 month. (Exact timeout may change depending on user feedback.)
- Alert threads are not redacted, but marked resolved by adding a quick reaction to the root event. (✅)
- All location threads are redacted, when the user closes the last one.
- New location threads cannot be created, until the user closes the last one.
- Location threads **are not** automatically redacted, when they are past the defined tracking time.

## Cleanup on Fresh Start
- When the app starts, it will check, if any location sharing threads of that user are still available in the room.
    - All location threads except the newest one are redacted.
    - The published end timestamp of the newest thread is evaluated.
    - If the end timestamp is *after* now, it will immediately continue to share the location, if that, for whatever reason, was stopped. (E.g. by the OS)


## Display at receiving users
- Alert status and location tracking data is gathered on a `Member` object.
- If a `Member` has either unredacted location tracking data, or an alert thread which is either unresolved or resolved less than 6 hours ago (timeout may be changed due to user feedback), there's a list item shown in the `now` scene:
  - The list item is a red alert warning, if there's an open alert thread. Location tracking will be shown as part of a detailed alert scene.
  - The list item is green, if the alert was resolved.
  - If there's unredacted location tracking data, it will have its own list item, until the sending user redacts it.
- In the room chat view, old alert status threads can be seen,
- as well as unredacted location tracking threads.
- Tapping the threads should open the same detail scenes, as from the `now` scene.
