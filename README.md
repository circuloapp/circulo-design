# Circulo Design
Phase 3 Figma file is here: https://www.figma.com/file/WwP8eyQKYhMcM5EIfcy7OK/circulo_p03?type=design&node-id=325%3A143&mode=design&t=PPj9rHYHpiQ5fabf-1


-------------------------------------------------------------
**ARCHIVE** [All of the work below is from Phase 2]

**Design File**
The Master Figma File is here: https://www.figma.com/file/NMllH5okXX0gaNbFvJ6W9t/circulo_p2.v1?node-id=1220%3A6772
The page '✅ M - Main Views' contains the designs ready for implementation
The other pages in that file contain design work in progress.

**Prototypes**

- Flow A: Main Navigation: https://www.figma.com/proto/NMllH5okXX0gaNbFvJ6W9t/circulo_p2.v1?page-id=1220%3A6772&node-id=1246%3A2695&viewport=245%2C48%2C0.26&scaling=min-zoom&starting-point-node-id=1246%3A2695&show-proto-sidebar=1
 
- Flow B: Flow B: Post a New Status: https://www.figma.com/proto/NMllH5okXX0gaNbFvJ6W9t/circulo_p2.v1?page-id=1220%3A6772&node-id=1343%3A3919&viewport=245%2C48%2C0.26&scaling=min-zoom&starting-point-node-id=1343%3A3781&show-proto-sidebar=1


- Flow C: Onboarding - No Circle: https://www.figma.com/proto/NMllH5okXX0gaNbFvJ6W9t/circulo_p2.v1?page-id=1220%3A6772&node-id=1760%3A4382&viewport=245%2C48%2C0.26&scaling=min-zoom&starting-point-node-id=1760%3A4382&show-proto-sidebar=1
