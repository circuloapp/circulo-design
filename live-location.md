We are building for low-end devices and users who are concerned about battery life. So we need to be mindful of preserving battery life while being able to provide accurate location information when needed. At a basic level, there are two ways to get location information from a mobile device.*

- Precise: The app can tell your device’s exact location.
- Approximate location: The app can tell that your device is within an area of about 3 square kilometers. Though, as we currently understand, it may not update as frequently as ‘precise’.

*Note: There are several nuances to understand about implementation variances for each platform. 

Throughout our conversations, three functional cases have emerged. They represent different ways we can think about delivering location information that are mindful about battery life.
- Emergency: Share precise location for a short time
- Ambient: Share approximate location for a longer time
- On Demand: Share or get precise location for a peer when asked by the user

WhatsApp live location, Life 360 and Google Family Link have been useful precedents.

Challenges:

Precise location sharing for an extended period of time will drain the phone battery, especially on low-end devices. We anticipate that precise location can be shared for up to an hour without having a significant effect. Due to this constraint, we will need to find clever ways to provide accurate information when required in various circumstances.

One of the most challenging cases we will face is providing accurate information in an emergency if the user is sharing an approximate location. If that person goes missing, the last location update will be only as accurate as the device operating system is able to provide. The way it updates approximate locations is fairly unpredictable.

We’ve discovered that the app could do a one time lookup of the precise location occasionally or if requested by someone in the circle. This affords us some opportunity. However, several factors will be at play in this situation—the phone of the missing person may have been turned off, it may not have access to the internet, or it may have been tampered with if it was confiscated.
